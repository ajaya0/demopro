from django.contrib import admin
from .models import Employee

@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('name', 'emp_id', 'department')
    search_fields = ('name', 'emp_id', 'department')
    list_filter = ('department',)
