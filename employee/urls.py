# employee/urls.py
from django.urls import path
from .views import add_employee, employee_list

from . import views

urlpatterns = [
    # for admin panel
    path('admin-dashboard/add-employee/', add_employee, name='add_employee'),
    path('admin-dashboard/employee-list/', employee_list, name='employee_list'),


    # for employee panel
    path("employee_dashboard/profile-view/",views.view_profile,name="profile-view"),
    # path("faq_view/",views.faq_view,"newApp/faqs.html"),
]
