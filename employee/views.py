# employee/views.py
from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import Employee
from .forms import UserForm, EmployeeForm

def is_hr(user):
     return user.groups.filter(name='HR').exists() or user.is_superuser

@login_required
@user_passes_test(is_hr)
def add_employee(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        name = request.POST.get('name')
        emp_id = request.POST.get('empid')
        address = request.POST.get('address')
        email=request.POST.get('email')
        department = request.POST.get('department')
        profile_pic = request.FILES.get('profile_pic')

        # Create User
        user = User.objects.create_user(username=username, password=password,email=email)
        
        # Create Employee
        employee = Employee(user=user, name=name, emp_id=emp_id, address=address,department=department, profile_image=profile_pic)
        employee.save()

        return redirect('employee_list')
    
    return render(request, 'employee/add_employee.html')

@login_required
@user_passes_test(is_hr)
def employee_list(request):
    employees = Employee.objects.all()
    return render(request, 'employee/employee_list.html', {'employees': employees})

def view_profile(request):
    employee = get_object_or_404(Employee, user=request.user)
    return render(request,"employee/employee_profile.html",{"employee":employee})

# def faq_view(request):
#     return render(request,"newApp/faqs.html")