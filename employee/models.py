# employee/models.py

from django.db import models
from django.contrib.auth.models import User

class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_image = models.ImageField(upload_to='profile_images/',null=True)
    DEPARTMENT_CHOICES = [
        ('developer', 'Developer'),
        ('tester', 'Tester'),
        ('sales', 'Sales'),
    ]
    department = models.CharField(max_length=20, choices=DEPARTMENT_CHOICES, null=True, blank=True)
    emp_id = models.CharField(max_length=10, unique=True, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    password = models.CharField(max_length=128, null=True, blank=True)
    address=models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.user.username
