# Generated by Django 4.1.13 on 2024-07-09 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='employee',
            name='address',
        ),
        migrations.RemoveField(
            model_name='employee',
            name='mobile',
        ),
        migrations.RemoveField(
            model_name='employee',
            name='profile_pic',
        ),
        migrations.AddField(
            model_name='employee',
            name='emp_id',
            field=models.CharField(blank=True, max_length=10, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='password',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='profile_image',
            field=models.ImageField(null=True, upload_to='profile_images/'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='department',
            field=models.CharField(blank=True, choices=[('developer', 'Developer'), ('tester', 'Tester'), ('sales', 'Sales')], max_length=20, null=True),
        ),
    ]
