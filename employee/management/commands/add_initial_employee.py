from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from employee.models import Employee

class Command(BaseCommand):
    help = 'Add initial data for Employee'

    def handle(self, *args, **kwargs):
        user = User.objects.create_user(username='employeeuser', password='password123')
        Employee.objects.create(
            user=user,
            name='John Doe',
            emp_id='E001',
            department='Development',   
            profile_image='profile_images/john_doe.jpg',  # Update with correct path if needed
            password='password123'
        )
        self.stdout.write(self.style.SUCCESS('Successfully added initial employee data'))
