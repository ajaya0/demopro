from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.indexpage, name='indexpage'),
    path('employeelogin/', views.login_user, name='login_user'),
    path("employee_dashboard/", views.employee_dashboard, name="employee_dashboard"),

    path('logout/', views.logout_user, name='logout_user'),
    
    path('adminlogin/', views.admin_login, name="admin_login"),
    # path('dashboard/', views.admin_dashboard, name='admin_dashboard'),
    path('admin-dashboard/', views.admin_dashboard_view,name='admin-dashboard'),
    path('admin-dashboard/faqs-view/',views.faqs_view,name="faq-view"),
] 