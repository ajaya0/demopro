from django.shortcuts import render, redirect
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from employee import models as EMODEL
from hr import models as HMODEL
import datetime

# Create your views here.

def indexpage(request):
    return render(request, "newApp/index.html")

def login_user(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        if not all([username, password]):
            messages.error(request, "Both username and password are required.")
            return redirect('login_user')

        user = authenticate(request, username=username, password=password)
        
        if user is not None and not user.is_superuser:
            login(request, user)
            fname=user.first_name
            print(fname)
            return redirect('employee_dashboard')
        else:
            messages.error(request, "Invalid Emp. username or password.")
            return redirect('login_user')

    return render(request, 'employee/employee_login.html')

def logout_user(request):
    logout(request)
    return redirect('indexpage/')


def admin_login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        if not all([username, password]):
            messages.error(request, "Admin username and password are required.")
            return redirect('admin_login')  # Change to 'admin_login'

        user = authenticate(request, username=username, password=password)
        
        if user is not None and user.is_superuser:
            login(request, user)
            return redirect('admin-dashboard')  # Change to 'admin-dashboard'
        else:
            messages.error(request, "Invalid Admin username or password.")
            return redirect('admin_login')  # Change to 'admin_login'

    return render(request, 'newApp/admin_login.html')


# @login_required
# def admin_dashboard(request):
#     return render(request,'newApp/adminbase.html')

@login_required
def employee_dashboard(request):
    return render(request,'employee/employee_dashboard.html',{'fname': request.user.first_name})


def is_employee(user):
    return user.groups.fillter(name='Employee').exits() 

@login_required(login_url="adminlogin")
def admin_dashboard_view(request):

    context = {
       'total_employee': EMODEL.Employee.objects.all().count(),
        'total_hr': HMODEL.HR.objects.all().count(),  # Assuming you have an HR model
        # 'pending_leaves': EMODEL.Leave.objects.filter(status='Pending').count(),  # Assuming you have a Leave model

        'employee':
        [
            {
                'id': 1,
                'firstname': 'Emil',
                'email': 'exam@gamil.com',
                'phone': 5551234,
                'joined_date': datetime.date(2022, 1, 5),
                'status':'Pending..'
            },
            {
                'id': 2,
                'firstname': 'Tobias',
                'email': 'exam@gamil.com',
                'phone': 5557777,
                'joined_date': datetime.date(2021, 4, 1),
                'status':'Pending..'
            },
            {
                'id':3,
                'firstname': 'Linus',
                'email': 'exam@gamil.com',
                'phone': 5554321,
                'joined_date': datetime.date(2021, 12, 24),
                'status':'Pending..'
            },
            {
                'id': 4,
                'firstname': 'Lene',
                'email': 'exam@gamil.com',
                'phone': 5551234,
                'joined_date': datetime.date(2021, 5, 1),
                'status':'Pending..'
            },
            {
                'id': 5,
                'firstname': 'Stalikken',
                'email': 'exam@gamil.com',
                'phone': 5559876,
                'joined_date': datetime.date(2022, 9, 29),
                'status':'Pending..'
            }
        ]
    }
    return render(request, 'newApp/admin_dashboard.html', context)

def faqs_view(request):
    return render(request,"newApp/faqs.html")
