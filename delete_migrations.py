import os
import glob

migrations_paths = glob.glob('*/migrations/*.py')
migrations_paths += glob.glob('*/migrations/*.pyc')

for migration_path in migrations_paths:
    if not migration_path.endswith('__init__.py'):
        os.remove(migration_path)
        print(f'Deleted {migration_path}')
