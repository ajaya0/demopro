# hr/forms.py

from django import forms
from django.contrib.auth.models import User
from .models import HR

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password', 'first_name', 'last_name', 'email']

class HRForm(forms.ModelForm):
    class Meta:
        model = HR
        fields = ['profile_image', 'emp_id', 'name']
