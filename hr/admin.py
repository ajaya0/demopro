from django.contrib import admin
from .models import HR

@admin.register(HR)
class HRAdmin(admin.ModelAdmin):
    list_display = ('name', 'emp_id')
    search_fields = ('name', 'emp_id')
