# hr/urls.py

from django.urls import path
from .views import add_hr, hr_list

urlpatterns = [
    path('admin-dashboard/add-hr/', add_hr, name='add-hr'),
    path('admin-dashboard/hr-list/', hr_list, name='hr_list'),
]

