from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from hr.models import HR

class Command(BaseCommand):
    help = 'Add initial data for HR'

    def handle(self, *args, **kwargs):
        user = User.objects.create_user(username='hruser', password='password123')
        HR.objects.create(
            user=user,
            name='Jane Smith',
            emp_id='H001',
            profile_image='profile_images/jane_smith.jpg',  # Update with correct path if needed
            password='password123'
        )
        self.stdout.write(self.style.SUCCESS('Successfully added initial HR data'))
