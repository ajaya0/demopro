# hr/views.py
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from .models import HR
from .forms import UserForm, HRForm
from django.contrib.auth import authenticate, login

def is_admin(user):
    return user.is_superuser

@login_required
@user_passes_test(is_admin)
def add_hr(request):
    if request.method == 'POST':
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        username = request.POST.get('username')
        password = request.POST.get('password')
        name = request.POST.get('name')
        emp_id = request.POST.get('emp_id')
        email = request.POST.get('email')
        profile_pic = request.FILES.get('profile_image')

        user = User.objects.create_user(
            username=username,
            password=password,
            email=email,
            first_name=first_name,
            last_name=last_name
        )
        hrdetails = HR(user=user, name=name, emp_id=emp_id, profile_image=profile_pic)
        hrdetails.save()
        return redirect('hr_list')
    return render(request, 'hr/add_hr.html')

@login_required
@user_passes_test(is_admin)
def hr_list(request):
    hrs = HR.objects.all()
    return render(request, 'hr/hr_list.html', {'hrs': hrs})
